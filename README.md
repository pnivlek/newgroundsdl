# newgroundsdl
Downloads all of the songs on a Newgrounds audio page.

Usage: newgroundsdl **AUDIO_PAGE_URL**

This is a python package. You can install it with pip:

```
pip3 install newgroundsdl
```

You can also use it as a library:

```
import newgroundsdl
newgroundsdl...
```

The source is fairly self-explanatory, so at this time I will not be writing documentation for the library
